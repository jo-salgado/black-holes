# import unittest
# from black_holes.core import BlackHoleBase
# from black_holes.prefabs import DatabaseBlackHole
# from black_holes.models import db
#
#
# class WormholesBaseTestCases(unittest.TestCase):
#     """Testing Wormholes Base"""
#
#     def setUp(self):
#         self.wormhole = BlackHoleBase()
#
#     def test_wormhole_base_instance_not_yet_implemented(self):
#         _wormhole = BlackHoleBase()
#         # Not implemented yet
#         self.assertRaises(KeyError, self.wormhole.get, 'test-key')
#         self.assertRaises(KeyError, self.wormhole.add, 'test-key', 'test-value')
#         self.assertRaises(KeyError, self.wormhole.update, 'test-key', 'test-value')
#         self.assertRaises(KeyError, self.wormhole.delete, 'test-key')
#         self.assertRaises(IOError, self.wormhole.fetch)
#         self.assertRaises(KeyError, self.wormhole.bulk, {'test_key': 'test_value'})
#
#     def test_wormholes_getter_setter_deleter(self):
#         """Testing Magic Set, Get and Delete methods."""
#         # implicit set
#         # set attr method
#         with self.assertRaises(KeyError):
#             self.wormhole.my_test_key = 'my_test_value'
#         # set item method
#         with self.assertRaises(KeyError):
#             self.wormhole['my_test_key'] = 'my_test_value'
#         # del attr method
#         with self.assertRaises(KeyError):
#             del self.wormhole.my_test_key
#         # del item method
#         with self.assertRaises(KeyError):
#             del self.wormhole['my_test_key']
#         # get attr method
#         with self.assertRaises(KeyError):
#             foo = self.wormhole.my_test_key
#         # get item method
#         with self.assertRaises(KeyError):
#             foo = self.wormhole['my_test_key']
#         # Bad bulk load arguments types
#         self.assertRaises(TypeError, self.wormhole.bulk, 'invalid-bulk')
#
#
# class LocalWormholeTestCases(unittest.TestCase):
#     """Testing Local Wormhole."""
#
#     def setUp(self):
#         self.local_wormhole = DatabaseBlackHole()
#
#     def tearDown(self):
#         db.close()
#
#     def test_add_key_to_wormhole(self):
#         self.local_wormhole.my_test_key = 'hallo-welt'
#         self.assertEqual(self.local_wormhole['my_test_key'], 'hallo-welt')
#
#     def test_add_bulk(self):
#         test_keys = {
#             'key1': 'value1',
#             'key2': 'value2',
#             'key3': 'value3'
#         }
#         self.local_wormhole.bulk(test_keys)
#         for k, v in test_keys.items():
#             self.assertEqual(self.local_wormhole[k], v)
#
#     def test_get_undefined_key(self):
#         """Test retrieve an undefined key."""
#         with self.assertRaises(IndexError):
#             print(self.local_wormhole.this_var_does_not_exists)
#
#     def test_wormholes_iterator(self):
#         all_keys = self.local_wormhole.fetch()
#         for i, k in enumerate(self.local_wormhole):
#             self.assertEqual(all_keys[i].get('key'), list(k.keys())[0])
#             self.assertEqual(all_keys[i].get('value'), list(k.values())[0])
#
#     def test_delete_key(self):
#         """Testing Delete LocalWormhole method."""
#         # Create a new var
#         self.local_wormhole['remove'] = 'me'
#         # Check new var exists
#         self.assertEqual(self.local_wormhole.remove, 'me')
#         # Delete key
#         del self.local_wormhole['remove']
#         # check key is really erased
#         with self.assertRaises(IndexError):
#             print(self.local_wormhole['remove'])
#         # try to delete an undefined key
#         with self.assertRaises(IndexError):
#             del self.local_wormhole['remove']
